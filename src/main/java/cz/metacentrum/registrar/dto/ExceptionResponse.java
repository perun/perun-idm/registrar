package cz.metacentrum.registrar.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.Instant;
import lombok.Data;

/**
 * Defines body of the HTTP response following an exception.
 */
@Data
public class ExceptionResponse {
  @Schema(description = "timestamp of error", example = "2023-10-15T15:40:23.947911375Z")
  private Instant timestamp;
  @Schema(description = "HTTP status code", example = "404")
  private int status;
  @Schema(description = "error message", example = "From not found")
  private String error;
  @Schema(description = "URL path", example = "/forms/1")
  private String path;

  /**
   * Basic constructor.
   *
   * @param status HTTP status code
   * @param error error message
   * @param path URL path
   */
  public ExceptionResponse(int status, String error, String path) {
    this.timestamp = Instant.now();
    this.status = status;
    this.error = error;
    this.path = path;
  }
}
