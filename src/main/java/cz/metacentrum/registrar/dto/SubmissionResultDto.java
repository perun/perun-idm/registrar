package cz.metacentrum.registrar.dto;

import java.util.List;
import lombok.Data;

/**
 * Data Transfer Object of SubmissionResult.
 */
@Data
public class SubmissionResultDto {
  private SubmissionDto submission;
  private List<String> messages;
  private SubmissionDto redirectSubmission;
  private String redirectUrl;
}
