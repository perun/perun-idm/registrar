package cz.metacentrum.registrar.security;

import java.util.Set;
import java.util.UUID;
import lombok.Data;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2TokenIntrospectionClaimAccessor;

/**
 * Representation of the users calling Registrar API.
 * Holds information about the users themselves + their authentication and rights.
 */
@Data
public abstract class RegistrarPrincipal
    implements OAuth2AuthenticatedPrincipal, OAuth2TokenIntrospectionClaimAccessor {

  protected Set<UUID> idmGroups;
  protected Set<Long> formManager;
  protected Set<Long> formApprover;

  public abstract boolean isAuthenticated();

  public abstract String getName();

  public abstract String getId();

  public abstract boolean isMfa();
}
