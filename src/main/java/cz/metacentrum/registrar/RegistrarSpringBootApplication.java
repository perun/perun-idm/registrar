package cz.metacentrum.registrar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot application init class.
 */
@SpringBootApplication
public class RegistrarSpringBootApplication {
  private RegistrarSpringBootApplication() {}

  public static void main(String[] args) {
    SpringApplication.run(RegistrarSpringBootApplication.class, args);
  }

}
