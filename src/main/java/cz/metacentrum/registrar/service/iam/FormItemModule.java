package cz.metacentrum.registrar.service.iam;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cz.metacentrum.registrar.model.FormItem;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents a different form item specific to the integrated IAM system.
 * The class properties specify the item’s configuration options and what values can be configured to them.
 * If a field is null, form manager can set any value.
 * If a field is not null, form manager can set only one of values from list.
 * For example:
 *  - if the regex property contains a list of two string values, form managers can only set these two values as
 *  regex for this form item.
 *  - If the regex property is an empty list, form managers cannot set any regex for this form item.
 *  - If the regex property has a null value, form managers can choose any regex value for this item.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class FormItemModule {

  private String id;
  private String displayName;
  private List<String> iamSourceAttributes;
  private List<String> sourceIdentityAttributes;
  private String iamDestinationAttribute;
  private List<String> prefilledStaticValue;
  private Map<Locale, List<String>> label;
  private List<FormItem.Type> itemTypes;
  private List<Boolean> updatable;
  private List<String> regex;
  private List<FormItem.Disabled> disabled;
  private List<FormItem.Hidden> hidden;
}
