package cz.metacentrum.registrar.service.iam;

import cz.metacentrum.registrar.model.Form;
import cz.metacentrum.registrar.model.SubmittedForm;
import java.util.List;
import java.util.Map;

/**
 * Represents module which can be assigned to a form. Contains logic that will be executed after form life-cycle events,
 * such as before approval. IAM systems specify their own modules by implementing this interface.
 */
public interface FormModule {
  /**
   * Returns list of config option names that must be set.
   */
  List<String> getConfigOptions();

  /**
   * Module's logic that should be executed before a submitted form is approved.
   *
   * @param submittedForm form
   */
  void beforeApprove(SubmittedForm submittedForm);

  /**
   * Module's logic that should be executed when a submitted form is approved.
   *
   * @param submittedForm form
   */
  void onApprove(SubmittedForm submittedForm, Map<String, String> configOptions);

  /**
   * Module's logic that should be executed when a submitted form is rejected.
   *
   * @param submittedForm form
   */
  void onReject(SubmittedForm submittedForm);

  /**
   * Module's logic that should be executed when a submitted form is loaded.
   *
   * @param submittedForm form
   */
  List<SubmittedForm> onLoad(SubmittedForm submittedForm, Map<String, String> configOptions);

  /**
   * Validates that a caller has right to assign this module to the form
   * with the given config options.
   *
   * @param form form
   * @param configOptions module configuration options
   * @return true if the caller has right to assign this module
   */
  boolean hasRightToAddToForm(Form form, Map<String, String> configOptions);
}
