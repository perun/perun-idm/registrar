package cz.metacentrum.registrar.config;

import java.util.concurrent.Executor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Configuration class defining execution of tasks.
 */
@Configuration
public class AsyncTasksConfig {

  /**
   * Instantiates executor for async tasks.
   *
   * @return ThreadPoolTaskExecutor
   */
  @Bean
  public Executor asyncExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(5);
    executor.setMaxPoolSize(10);
    executor.setQueueCapacity(500);
    executor.setThreadNamePrefix("asyncExecutor-");
    executor.initialize();
    return executor;
  }
}
