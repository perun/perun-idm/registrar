package cz.metacentrum.registrar.controller.aop;

import cz.metacentrum.registrar.dto.ExceptionResponse;
import cz.metacentrum.registrar.exception.EntityNotFoundException;
import cz.metacentrum.registrar.exception.ValidationException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Implements translating thrown exceptions into meaningful uniform API responses.
 * For example, if an exception of the class FormNotFoundException is thrown in the API layer, it will be returned as
 * an HTTP response with the status code 404 Not Found and a JSON message.
 * Implemented using Spring’s support for aspect-oriented programming.
 */
@ControllerAdvice
public class CustomResponseEntityExceptionHandler {
  /**
   * Handles EntityNotFoundException.
   *
   * @param ex exception to handle
   * @param httpRequest original HTTP request
   * @return resulting HTTP response
   */
  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ExceptionResponse> formNotFoundHandler(EntityNotFoundException ex,
                                                               HttpServletRequest httpRequest) {
    ExceptionResponse response = new ExceptionResponse(HttpStatus.NOT_FOUND.value(),
        ex.getMessage(), httpRequest.getRequestURI());
    return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
  }

  /**
   * Handles AccessDeniedException.
   *
   * @param ex exception to handle
   * @param httpRequest original HTTP request
   * @return resulting HTTP response
   */
  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<ExceptionResponse> accessDeniedExceptionHandler(AccessDeniedException ex,
                                                                        HttpServletRequest httpRequest) {
    ExceptionResponse response = new ExceptionResponse(HttpStatus.FORBIDDEN.value(),
        ex.getMessage(), httpRequest.getRequestURI());
    return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
  }

  /**
   * Handles MethodArgumentNotValidException.
   *
   * @param ex exception to handle
   * @param httpRequest original HTTP request
   * @return resulting HTTP response
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ExceptionResponse> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException ex,
                                                                                  HttpServletRequest httpRequest) {
    ExceptionResponse response = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(),
        ex.getMessage(), httpRequest.getRequestURI());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handles any ValidationException.
   *
   * @param ex exception to handle
   * @param httpRequest original HTTP request
   * @return resulting HTTP response
   */
  @ExceptionHandler(ValidationException.class)
  public ResponseEntity<ExceptionResponse> genericExceptionHandler(ValidationException ex,
                                                                   HttpServletRequest httpRequest) {
    ExceptionResponse response = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(),
        ex.getMessage(), httpRequest.getRequestURI());
    return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handles any other generic Exception.
   *
   * @param ex exception to handle
   * @param httpRequest original HTTP request
   * @return resulting HTTP response
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<ExceptionResponse> genericExceptionHandler(Exception ex, HttpServletRequest httpRequest) {
    ExceptionResponse response = new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        ex.getMessage(), httpRequest.getRequestURI());
    return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
