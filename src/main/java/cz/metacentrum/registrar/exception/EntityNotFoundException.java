package cz.metacentrum.registrar.exception;

/**
 * Thrown when entity is not found in the underlying datasource.
 */
public class EntityNotFoundException extends RuntimeException {
  public EntityNotFoundException(String message) {
    super(message);
  }
}
