package cz.metacentrum.registrar.exception;

/**
 * Thrown when request validation fails.
 */
public class ValidationException extends RuntimeException {
  public ValidationException(String message) {
    super(message);
  }
}
