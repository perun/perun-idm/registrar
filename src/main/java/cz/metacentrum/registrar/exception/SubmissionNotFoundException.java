package cz.metacentrum.registrar.exception;

/**
 * Thrown when Submission is not found in the underlying datasource.
 */
public class SubmissionNotFoundException extends EntityNotFoundException {
  public SubmissionNotFoundException(Long id) {
    super("Could not find submission with id: " + id);
  }

  public SubmissionNotFoundException(String message) {
    super("Could not find submission: " + message);
  }
}
