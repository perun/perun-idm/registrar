package cz.metacentrum.registrar.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Represents a form submitted by a user.
 * It holds information about which form type was submitted (initial or extension)
 * and also whether it was submitted as part of an advanced flow.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubmittedForm {

  @Id
  @GeneratedValue
  @Nullable
  private Long id;

  @Column
  private Integer stepOrder;

  @NonNull
  @ManyToOne
  private Submission submission;

  @Column
  @Nullable
  private String redirectUrl;

  @NonNull
  @ManyToOne
  @JoinColumn(name = "form_id")
  private Form form;

  @Enumerated(EnumType.STRING)
  private Form.FormType formType;

  @Enumerated(EnumType.STRING)
  private FormState formState;

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "submitted_form_id")
  private List<FormItemData> formData;

  @Column
  @Nullable
  private AssignedFlowForm.FlowType flowType;
}
