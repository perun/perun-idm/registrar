package cz.metacentrum.registrar.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 * Represents a result of submitting a form.
 * Meant to inform submitters about the current state of the submission process and possible next steps.
 */
@Data
public class SubmissionResult {
  private Submission submission;
  private List<String> messages = new ArrayList<>();
  private Submission redirectSubmission;
  private String redirectUrl;

  public void addMessage(String message) {
    messages.add(message);
  }
}
