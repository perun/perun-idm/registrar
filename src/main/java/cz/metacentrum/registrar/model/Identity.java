package cz.metacentrum.registrar.model;

import lombok.Data;

/**
 * Represent identities from IAM systems that are similar to a submitter’s authenticated identity and, thus, are
 * possible candidates for the identity consolidation process.
 * UI should then display them to the submitters and possibly redirect them to the Consolidator application.
 */
@Data
public class Identity {

  private String name;
  private String organization;
  private String email;
  private IdentityType type;

  /**
   * Type of the identity.
   */
  public enum IdentityType {
    IDP,
    PASSWORD,
    CERT
  }
}
