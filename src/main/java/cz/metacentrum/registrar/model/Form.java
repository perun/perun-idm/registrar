package cz.metacentrum.registrar.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

/**
 * Represents a form and its basic configuration.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Form {

  @Id
  @GeneratedValue
  private Long id;

  @Column
  private UUID iamObject;

  @Column
  private UUID iamFormManagersGroup;

  @Column
  private String name;

  @Column(unique = true)
  private String urlSuffix;

  @Column
  @Nullable
  private String redirectUrl;

  @Column
  private boolean canBeResubmitted;

  @Column
  private boolean autoApprove;

  /**
   * Represents the two variants each form can have - initial and extension.
   * Each variant can display different form items or execute different flows.
   */
  public enum FormType { INITIAL, EXTENSION }
}
