package cz.metacentrum.registrar.model;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Represents a flow relationship between two forms.
 * Specifies which form is the main one and which is the flow one, as well as the conditions that need to be fulfilled
 * to execute the flow event.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignedFlowForm {

  @Id
  @GeneratedValue
  private Long id;

  @Enumerated(EnumType.STRING)
  private FlowType flowType;

  @Column
  private int ordnum;

  @ManyToOne
  @JoinColumn(name = "flow_form_id")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Form flowForm;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "main_form_id")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Form mainForm;

  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "flow_form_form_types")
  @JoinColumn(name = "assigned_flow_form_id")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<Form.FormType> ifFlowFormType = Arrays.asList(Form.FormType.INITIAL, Form.FormType.EXTENSION);

  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "main_form_form_types")
  @JoinColumn(name = "assigned_flow_form_id")
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<Form.FormType> ifMainFlowType = Arrays.asList(Form.FormType.INITIAL, Form.FormType.EXTENSION);

  /**
   * Specifies type of the flow relationship.
   * AUTO - when users submit a main form, a flow form is automatically submitted on their behalf.
   * REDIRECT - when users submit a main form, they are redirected to a flow form
   * PRE - when users want to fill out a main form, a flow form is first displayed to them (for example VO application
   * before Group application in Perun).
   */
  public enum FlowType { PRE, AUTO, REDIRECT }
}
