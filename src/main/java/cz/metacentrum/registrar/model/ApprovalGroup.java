package cz.metacentrum.registrar.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Represents a group from an IAM system that is configured as a form’s approvers.
 * The approval process can be further configured (e.g. minimal number of approvers needed from this group).
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApprovalGroup implements Comparable<ApprovalGroup> {
  @Id
  @GeneratedValue
  private Long id;

  @ManyToOne
  @JoinColumn(name = "form_id")
  @JsonIgnore
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Form form;

  @Column
  private int level;

  @Column
  private boolean mfaRequired;

  @Column
  private int minApprovals;

  @Column
  private UUID iamGroup;

  @Override
  public int compareTo(ApprovalGroup o) {
    return Integer.compare(level, o.level);
  }
}
