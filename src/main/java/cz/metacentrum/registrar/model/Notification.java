package cz.metacentrum.registrar.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapKeyColumn;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Definition for mail notifications sent throughout a form's life cycle.
 * Different (localized) (HTML) notifications can be defined for different events and recipients.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notification {

  @Id
  @GeneratedValue
  private Long id;

  @ManyToOne
  @JoinColumn(name = "form_id")
  @JsonIgnore
  private Form form;

  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "notification_form_types")
  @JoinColumn(name = "notification_id")
  @Fetch(FetchMode.SUBSELECT)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<Form.FormType> formTypes = Arrays.asList(Form.FormType.INITIAL, Form.FormType.EXTENSION);

  @Column
  private boolean enabled;

  @Enumerated(EnumType.STRING)
  private Event event;

  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "notification_recipients")
  @JoinColumn(name = "notification_id")
  @Fetch(FetchMode.SUBSELECT)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private List<Recipient> recipients;

  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "notification_messages")
  @Fetch(FetchMode.SUBSELECT)
  @MapKeyColumn(name = "message_locale")
  @Column(name = "message_content")
  private Map<Locale, String> localizedMessages;

  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "notification_html_messages")
  @Fetch(FetchMode.SUBSELECT)
  @MapKeyColumn(name = "message_locale")
  @Column(name = "message_content")
  private Map<Locale, String> localizedHtmlMessages;

  /**
   * Type of event for which to send the notification.
   */
  public enum Event { FORM_SUBMITTED, MAIL_VALIDATION, FORM_APPROVED, FORM_REJECTED, ERROR, INVITATION }

  /**
   * To whom to send the notification.
   */
  public enum Recipient { SUBMITTER, MANAGER, APPROVER }
}
