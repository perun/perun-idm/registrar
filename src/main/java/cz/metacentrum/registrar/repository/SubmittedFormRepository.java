package cz.metacentrum.registrar.repository;

import cz.metacentrum.registrar.model.Form;
import cz.metacentrum.registrar.model.FormState;
import cz.metacentrum.registrar.model.SubmittedForm;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing SubmittedForms.
 */
@Repository
public interface SubmittedFormRepository extends JpaRepository<SubmittedForm, Long> {
  List<SubmittedForm> findSubmittedFormsByForm(Form form);

  List<SubmittedForm> findSubmittedFormsByFormAndFormState(Form form, FormState state);
}
