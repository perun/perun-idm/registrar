package cz.metacentrum.registrar.repository;

import cz.metacentrum.registrar.model.AssignedFormModule;
import cz.metacentrum.registrar.model.Form;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing FormModules.
 */
@Repository
public interface FormModuleRepository extends JpaRepository<AssignedFormModule, Long> {
  List<AssignedFormModule> getAllByForm(Form form);

}
