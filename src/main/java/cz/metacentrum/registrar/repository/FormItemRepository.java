package cz.metacentrum.registrar.repository;

import cz.metacentrum.registrar.model.Form;
import cz.metacentrum.registrar.model.FormItem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing FormItems.
 */
@Repository
public interface FormItemRepository extends JpaRepository<FormItem, Long> {
  List<FormItem> getAllByFormAndIsDeleted(Form form, boolean deleted);
}
