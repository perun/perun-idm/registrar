package cz.metacentrum.registrar.repository;

import cz.metacentrum.registrar.model.Approval;
import cz.metacentrum.registrar.model.SubmittedForm;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing Approvals.
 */
@Repository
public interface ApprovalRepository extends JpaRepository<Approval, Long> {
  List<Approval> findApprovalByLevelAndSubmittedFormAndDecision(int level, SubmittedForm submittedForm,
                                                                Approval.Decision decision);
}
