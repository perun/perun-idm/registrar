package cz.metacentrum.registrar.repository;


import cz.metacentrum.registrar.model.Role;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing Roles.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  @Query(
      value = "SELECT r.name FROM role r JOIN roles_users u ON r.id = u.role_id WHERE u.user_id = ?1",
      nativeQuery = true
  )
  List<String> getRolesByUserIdentifier(String userIdentifier);
}
