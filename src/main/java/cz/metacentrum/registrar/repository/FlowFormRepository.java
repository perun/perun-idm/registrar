package cz.metacentrum.registrar.repository;

import cz.metacentrum.registrar.model.AssignedFlowForm;
import cz.metacentrum.registrar.model.Form;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing FlowForms.
 */
@Repository
public interface FlowFormRepository extends JpaRepository<AssignedFlowForm, Long> {
  List<AssignedFlowForm> getAllByMainForm(Form mainForm);

}
