package cz.metacentrum.registrar.repository;

import cz.metacentrum.registrar.model.ApprovalGroup;
import cz.metacentrum.registrar.model.Form;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing ApprovalGroups.
 */
@Repository
public interface ApprovalGroupRepository extends JpaRepository<ApprovalGroup, Long> {
  List<ApprovalGroup> getAllByForm(Form form);

}
