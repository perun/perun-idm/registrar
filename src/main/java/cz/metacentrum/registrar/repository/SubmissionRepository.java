package cz.metacentrum.registrar.repository;

import cz.metacentrum.registrar.model.Submission;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository layer interface managing Submissions.
 */
@Repository
public interface SubmissionRepository extends JpaRepository<Submission, Long> {
  List<Submission> getAllBySubmitterId(String submitterId);

  List<Submission> getAllByOriginalIdentityIdentifierAndOriginalIdentityIssuer(
      String originalIdentityIdentifier,
      String originalIdentityIssuer);
}
